const rp = require('request-promise');

const options = {
    method: 'POST',
    uri: 'http://127.0.0.1:8080/health',
    body: {key: 'test'},
    json: true
};

rp(options)
    .then(function (parsedBody) {
        console.log('log is:', parsedBody);
    })
    .catch(function (err) {
        console.log('Health check error:', err);
    });