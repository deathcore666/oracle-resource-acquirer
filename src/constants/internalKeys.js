
const insideNetworkCall                  ="010";
const outsideNetworkCall                 ="020";
const internationalCall                  ="030";
const householdPhoneCall                 ="040";
const paidServiceCall                    ="050";
const freeServiceCall                    ="060";

const incomingInsideNetworkCall          ="011";
const incomingOutsideNetworkCall         ="021";
const incomingInternationalCall          ="031";
const incomingHouseholdPhoneCall         ="041";
const incomingPaidServiceCall            ="051";
const incomingFreeServiceCall            ="061";

const insideNetworkSms                   = "110";
const outsideNetworkSms                  = "120";
const internationalSms                   = "130";
const paidServiceSms                     = "140";
const freeServiceSms                     = "150";
const incomingInsideNetworkSms           = "111";
const incomingOutsideNetworkSms          = "121";
const incomingInternationalSms           = "131";
const incomingPaidServiceSms             = "141";
const incomingFreeServiceSms             = "151";

const incomingRoamingCall                = "071";
const roamingCall                        = "070";
const incomingRoamingSms                 = "161";
const roamingSms                         = "160";

const internet                           = "210";
const roamingInternet                    = "220";

const Bishkek                            = "FRU";
const Chui                               = "CHU";
const IssykKul                           = "ISK";
const Naryn                              = "NRN";
const Talas                              = "TLS";
const BATKEN                             = "BTK";
const Jalalabat                          = "JLB";
const Osh                                = "OSH";

const IDpassport                         = "ID_DOC_TYPE";
const InternationalPassport              = "IP_DOC_TYPE";
const DriversLicense                     = "DL_DOC_TYPE";
const BirthCertificate                   = "BC_DOC_TYPE";

module.exports = {
    insideNetworkCall,
    outsideNetworkCall,
    internationalCall,
    householdPhoneCall,
    paidServiceCall,
    freeServiceCall,

    incomingInsideNetworkCall,
    incomingOutsideNetworkCall,
    incomingInternationalCall,
    incomingHouseholdPhoneCall,
    incomingPaidServiceCall,
    incomingFreeServiceCall,

    insideNetworkSms,
    outsideNetworkSms,
    internationalSms,
    paidServiceSms,
    freeServiceSms,
    incomingInsideNetworkSms,
    incomingOutsideNetworkSms,
    incomingInternationalSms,
    incomingPaidServiceSms,
    incomingFreeServiceSms,
    incomingRoamingCall,
    roamingCall,
    incomingRoamingSms,
    roamingSms,
    internet,
    roamingInternet,

    Bishkek,
    Chui,
    IssykKul,
    Naryn,
    Talas,
    BATKEN,
    Jalalabat,
    Osh,

    IDpassport,
    InternationalPassport,
    DriversLicense,
    BirthCertificate,
};