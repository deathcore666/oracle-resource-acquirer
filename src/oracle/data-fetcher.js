const oracledb = require('oracledb');
const config = require('config');
const logger = require('askarteklogger');

const BEELINE = require('../constants/beeline');
const UsageTransformStream = require('../oracle/usage-data-transform');
const BalanceTopUpDataTransformStream = require('../oracle/balance-topup-data-transform');
const BalanceCreditDataTransformStream = require('../oracle/balance-credit-data-transform');
const BalanceRemainingDataTransformStream = require('../oracle/balance-remaining-data-transform');

/**
 * DataFetcher class designed for streaming and transforming raw Beeline Oracle data
 * into messages for the kafka Cluster, the structure of messages is defined in the
 * project specification.
 */

class DataFetcher {

    constructor(task) {
        this.connection = null;
        this.stream = null;
        this.transformStream = null;
        this.task = task;
        this.connection = null;

        this.columnsMap = {};
    }

    async connect() {
        try {
            let connectionConfigs = config.get('oracledb');
            this.connection = await oracledb.getConnection(connectionConfigs);
        } catch (err) {
            logger.logError('Error Connecting to Oracle ' + err, this.task.task_id);
            console.error('Error Connecting to Oracle ' + err + this.task.task_id);
        }
    }

    fetchUsageDataStream(query, params) {
        return new Promise(async (resolve, reject) => {
            if (!query)
                query = BEELINE.USAGE_QUERY;

            try {
                this.columnsMap = await this.buildDataMap(BEELINE.USAGE_QUERY);

                this.transformStream = new UsageTransformStream(this.columnsMap, this.task['task_id']);

                this.stream = await this.fetchDataStream(query);
                let result = this.stream.pipe(this.transformStream.transformer);
                resolve(result)

            } catch (err) {
                this.releaseConnection();
                reject('Error at fetchUsageDataStream. Error: ' + err);
            }
        })
    }

    fetchBalanceCreditDataStream(query, params) {
        return new Promise(async (resolve, reject) => {
            if (!query)
                query = BEELINE.BALANCE_CREDIT_QUERY;

            try {
                this.columnsMap = await this.buildDataMap(BEELINE.BALANCE_CREDIT_QUERY);

                this.transformStream = new BalanceCreditDataTransformStream(this.columnsMap, this.task['task_id']);

                this.stream = await this.fetchDataStream(query);
                let result = this.stream.pipe(this.transformStream.transformer);
                resolve(result)

            } catch (err) {
                this.releaseConnection();
                reject('Error at fetchBalanceCreditDataStream. Error: ' + err);
            }
        })
    }

    fetchBalanceTopUpDataStream(query, params) {
        return new Promise(async (resolve, reject) => {
            if (!query)
                query = BEELINE.BALANCE_TOPUP_QUERY;

            try {
                this.columnsMap = await this.buildDataMap(BEELINE.BALANCE_TOPUP_QUERY);

                this.transformStream = new BalanceTopUpDataTransformStream(this.columnsMap, this.task['task_id']);

                this.stream = await this.fetchDataStream(query);
                let result = this.stream.pipe(this.transformStream.transformer);
                resolve(result)

            } catch (err) {
                this.releaseConnection();
                reject('Error at fetchBalanceTopUpDataStream. Error: ' + err);
            }
        })
    }

    fetchBalanceRemainingDataStream(query, params) {
        return new Promise(async (resolve, reject) => {
            if (!query)
                query = BEELINE.BALANCE_REMAINING_QUERY;

            try {
                this.columnsMap = await this.buildDataMap(BEELINE.BALANCE_REMAINING_QUERY);

                this.transformStream = new BalanceRemainingDataTransformStream(this.columnsMap, this.task['task_id']);

                this.stream = await this.fetchDataStream(query);
                let result = this.stream.pipe(this.transformStream.transformer);
                resolve(result)

            } catch (err) {
                this.releaseConnection();
                reject('Error at fetchBalanceRemainingDataStream. Error: ' + err);
            }
        })
    }

    async buildDataMap(query) {
        let i = 0;
        let resultMap = {};

        return new Promise(async (resolve, reject) => {
            try {
                let res = await this.connection.execute(
                    query,
                    [],
                    {maxRows: 1});

                res['metaData'].map(x => resultMap[x.name] = i++);

                resolve(resultMap);
            }

            catch (err) {
                let error = new Error('Error building data map. Error: ' + err);
                error.code = -1;
                reject(error)
            }
        })
    }

    fetchDataStream(query, params = []) {
        return this.connection.queryStream(
            query,
            params,
            {fetchArraySize: 100});
    }

    releaseConnection() {
        const main = async () => {
            if (this.connection) {
                await this.connection.close();
                console.log('connection released:', this.task['task_id']);
            }
        };

        main()
            .catch(err => {
                logger.logError('Error closing connection:' + err, this.task['task_id']);
            })
    }
}

module.exports = DataFetcher;