const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class BalanceTopUpDataTransform {
    constructor(balanceTopUpColumnsMap, task_id) {
        this.transformBalanceTopUpData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                const EVENT_DT      = balanceTopUpColumnsMap[BEELINE.EVENT_DT];
                const RECHARGE_NVAL = balanceTopUpColumnsMap[BEELINE.RECHARGE_NVAL];
                const ACCOUNT_KEY   = balanceTopUpColumnsMap[BEELINE.ACCOUNT_KEY];

                let msg = {
                    data    : {
                        taskId          : task_id,
                        EOF: 0,
                        day             : (record[EVENT_DT].getDay() === 6 || record[EVENT_DT].getDay() === 0) ? 'weekend' : 'weekday',
                        hour            : record[EVENT_DT].getHours(),
                        date            : record[EVENT_DT].getDate(),
                        month           : record[EVENT_DT].getMonth(),
                        year            : record[EVENT_DT].getFullYear(),
                        topupAmount     : record[RECHARGE_NVAL],
                        accountId       : record[ACCOUNT_KEY]
                    },
                    topic: BEELINE.BALANCE_TOPUP_TOPIC,
                    key     : Date.now() + record[ACCOUNT_KEY],
                };

                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceTopUpData;
    }
}

module.exports = BalanceTopUpDataTransform;