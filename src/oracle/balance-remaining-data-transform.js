const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class BalanceRemainingDataTransform {
    constructor(balanceRemainingColumnsMap, task_id) {
        this.transformBalanceRemainingData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                const BALANCE_NVAL  = balanceRemainingColumnsMap[BEELINE.BALANCE_NVAL];
                const ACCOUNT_KEY   = balanceRemainingColumnsMap[BEELINE.ACCOUNT_KEY];
                const EVENT_DT      = balanceRemainingColumnsMap[BEELINE.EVENT_DT];

                let msg = { data: {
                        taskId          : task_id,
                        EOF: 0,
                        month           : record[EVENT_DT].getMonth(),
                        snapshotDate    : Date.now(),
                        snapshotValue   : record[BALANCE_NVAL],
                        accountId       : record[ACCOUNT_KEY]
                    },
                    topic: BEELINE.BALANCE_REMAINING_TOPIC,

                    key     : Date.now() + record[ACCOUNT_KEY],
                };
                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceRemainingData;
    }
}

module.exports = BalanceRemainingDataTransform;