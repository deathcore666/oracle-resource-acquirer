const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class BalanceCreditDataTransform {
    constructor(balanceCreditColumnsMap, task_id) {
        this.transformBalanceCreditData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                let msg = { data: {
                        taskId : task_id,
                        EOF: 0,
                        trustPaymentData: {
                            totalAmount     : record[balanceCreditColumnsMap['Выдача отчетный месяц']],
                            month1          : record[balanceCreditColumnsMap['Месяц возврата 1']],
                            month2          : record[balanceCreditColumnsMap['Месяц возврата 2']],
                            month3          : record[balanceCreditColumnsMap['Месяц возврата 3']],
                            month4          : record[balanceCreditColumnsMap['Месяц возврата 4']],
                            month5          : record[balanceCreditColumnsMap['Месяц возврата 5']],
                            month6          : record[balanceCreditColumnsMap['Месяц возврата 6']],
                            month7          : record[balanceCreditColumnsMap['Месяц возврата 7']],
                            month8          : record[balanceCreditColumnsMap['Месяц возврата 8']],
                            month9          : record[balanceCreditColumnsMap['Месяц возврата 9']],
                            month10         : record[balanceCreditColumnsMap['Месяц возврата 10']],
                            month11         : record[balanceCreditColumnsMap['Месяц возврата 11']],
                            month12         : record[balanceCreditColumnsMap['Месяц возврата 12']],
                        },
                        accountId   : record[balanceCreditColumnsMap[BEELINE.SUBS_KEY]]
                    },
                    topic: BEELINE.BALANCE_CREDIT_TOPIC,
                    key     : Date.now() + record[balanceCreditColumnsMap[BEELINE.SUBS_KEY]],
                };
                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceCreditData;
    }
}

module.exports = BalanceCreditDataTransform;