const {Transform} = require('stream');

const BEELINE = require('../constants/beeline');
const KEYS = require('../constants/internalKeys');

const callTypes = {
    'calls': [
        KEYS.insideNetworkCall,
        KEYS.outsideNetworkCall,
        KEYS.internationalCall,
        KEYS.householdPhoneCall,
        KEYS.paidServiceCall,
        KEYS.freeServiceCall,
        KEYS.incomingInsideNetworkCall,
        KEYS.incomingOutsideNetworkCall,
        KEYS.incomingInternationalCall,
        KEYS.incomingHouseholdPhoneCall,
        KEYS.incomingPaidServiceCall,
        KEYS.incomingFreeServiceCall,
    ],
    'sms': [
        KEYS.insideNetworkSms,
        KEYS.outsideNetworkSms,
        KEYS.internationalSms,
        KEYS.paidServiceSms,
        KEYS.freeServiceSms,
        KEYS.incomingInsideNetworkSms,
        KEYS.incomingOutsideNetworkSms,
        KEYS.incomingInternationalSms,
        KEYS.incomingPaidServiceSms,
        KEYS.incomingFreeServiceSms,
    ],
    'roamingCalls': [
        KEYS.incomingRoamingCall,
        KEYS.roamingCall
    ],
    'roamingSms': [
        KEYS.incomingRoamingSms,
        KEYS.roamingSms,
    ],
    'internet': [KEYS.internet],
    'roamingInternet': [KEYS.roamingInternet],
};

class UsageDataTransformStream {
    constructor(usageColumnsMap, task_id) {
        this.transformUsageData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                let msg = {};
                const CALL_TYPE_KEY = usageColumnsMap[BEELINE.CALL_TYPE_KEY];
                const COUNTRY_KEY = usageColumnsMap[BEELINE.COUNTRY_KEY];
                const B_COUNTRY_KEY = usageColumnsMap[BEELINE.B_COUNTRY_KEY];
                const CITY_KEY = usageColumnsMap[BEELINE.CITY_KEY];
                const B_CITY_KEY = usageColumnsMap[BEELINE.B_CITY_KEY];
                const REGION_KEY = usageColumnsMap[BEELINE.REGION_KEY];
                const B_REGION_KEY = usageColumnsMap[BEELINE.B_REGION_KEY];
                const START_DT = usageColumnsMap[BEELINE.START_DT];
                const CHARGE_NVAL = usageColumnsMap[BEELINE.CHARGE_NVAL];
                const PHONE_NUM = usageColumnsMap[BEELINE.PHONE_NUM];
                const B_PHONE_NUM = usageColumnsMap[BEELINE.B_PHONE_NUM];
                const CELL_NVAL = usageColumnsMap[BEELINE.CELL_NVAL];
                const B_CELL_NVAL = usageColumnsMap[BEELINE.B_CELL_NVAL];
                const IMEI_CVAL = usageColumnsMap[BEELINE.IMEI_CVAL];
                const SUBS_KEY = usageColumnsMap[BEELINE.SUBS_KEY];
                const DURATION_NVAL = usageColumnsMap[BEELINE.DURATION_NVAL];
                const DATA_VOL_NVAL = usageColumnsMap[BEELINE.DATA_VOL_NVAL];
                const PRICE_PLAN_KEY = usageColumnsMap[BEELINE.PRICE_PLAN_KEY];
                const USAGEID = usageColumnsMap[BEELINE.USAGEID];

                let usageCategory;
                let usageSubCategory;
                loop1:
                for (let category in callTypes) {
                    for (let index in callTypes[category]) {
                        if (callTypes[category].hasOwnProperty(index)) {
                            if (record[CALL_TYPE_KEY] === callTypes[category][index]) {
                                usageCategory = category;
                                usageSubCategory = callTypes[category][index];
                                break loop1;
                            }
                        }
                    }
                }

                //SMS_TOPIC
                if (usageCategory === 'sms') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            textTypeKey: usageSubCategory,
                            textPrice: record[CHARGE_NVAL],
                            phoneNumFrom: record[PHONE_NUM],
                            phoneNumTo: record[B_PHONE_NUM],
                            locationKeys: {
                                // CELL_NVAL       : record[CELL_NVAL],
                                // B_CELL_NVAL     : record[B_CELL_NVAL],
                                CITY_KEY: record[CITY_KEY],
                                B_CITY_KEY: record[B_CITY_KEY],
                                COUNTRY_KEY: record[COUNTRY_KEY],
                                B_COUNTRY_KEY: record[B_COUNTRY_KEY],
                                REGION_KEY: record[REGION_KEY],
                                B_REGION_KEY: record[B_REGION_KEY]

                            },
                            senderImei: record[IMEI_CVAL],
                        },
                        topic: BEELINE.SMS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }

                //SMS_TOPIC
                if (usageCategory === 'roamingSms') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            textTypeKey: usageSubCategory,
                            textPrice: record[CHARGE_NVAL],
                            phoneNumFrom: record[PHONE_NUM],
                            phoneNumTo: record[B_PHONE_NUM],
                            locationKeys: {
                                COUNTRY_KEY: record[COUNTRY_KEY],
                            },
                            senderImei: record[IMEI_CVAL],
                        },
                        topic: BEELINE.ROAMING_SMS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }

                //CALLS_TOPIC
                if (usageCategory === 'calls') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            callTypeKey: usageSubCategory,
                            callDuration: record[DURATION_NVAL],
                            callPrice: record[CHARGE_NVAL],
                            phoneNumFrom: record[PHONE_NUM],
                            phoneNumTo: record[B_PHONE_NUM],
                            locationKeys: {
                                // CELL_NVAL       : record[CELL_NVAL],
                                // B_CELL_NVAL     : record[B_CELL_NVAL],
                                CITY_KEY: record[CITY_KEY],
                                B_CITY_KEY: record[B_CITY_KEY],
                                COUNTRY_KEY: record[COUNTRY_KEY],
                                B_COUNTRY_KEY: record[B_COUNTRY_KEY],
                                REGION_KEY: record[REGION_KEY],
                                B_REGION_KEY: record[B_REGION_KEY]

                            },
                            callerImei: record[IMEI_CVAL],
                        },
                        topic: BEELINE.CALLS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }

                //ROAMING_CALLS_TOPIC
                if (usageCategory === 'roamingCalls') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            callTypeKey: usageSubCategory,
                            callDuration: record[DURATION_NVAL],
                            callPrice: record[CHARGE_NVAL],
                            phoneNumFrom: record[PHONE_NUM],
                            phoneNumTo: record[B_PHONE_NUM],
                            locationKeys: {
                                COUNTRY_KEY: record[COUNTRY_KEY],
                            },
                            callerImei: record[IMEI_CVAL],
                        },
                        topic: BEELINE.ROAMING_CALLS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }

                //Internet Logs
                if (usageCategory === 'internet') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            phoneNum: record[PHONE_NUM],
                            dataVolume: record[DATA_VOL_NVAL],
                            dataPrice: record[CHARGE_NVAL],
                            locationKeys: {
                                // CELL_NVAL       : record[CELL_NVAL],
                                CITY_KEY: record[CITY_KEY],
                                COUNTRY_KEY: record[COUNTRY_KEY],
                                REGION_KEY: record[REGION_KEY],
                            },
                            hostIp: 'N/A'
                        },
                        topic:BEELINE.INTERNET_LOGS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }

                // Roaming Internet Logs
                if (usageCategory === 'roamingInternet') {
                    msg = {
                        data: {
                            taskId: task_id,
                            EOF: 0,
                            day: (record[START_DT].getDay() === 6 || record[START_DT].getDay() === 0) ? 'weekend' : 'weekday',
                            hour: record[START_DT].getHours(),
                            date: record[START_DT].getDate(),
                            month: record[START_DT].getMonth(),
                            year: record[START_DT].getFullYear(),
                            phoneNum: record[PHONE_NUM],
                            dataVolume: record[DATA_VOL_NVAL],
                            dataPrice: record[CHARGE_NVAL],
                            locationKeys: {
                                COUNTRY_KEY: record[COUNTRY_KEY],
                            },
                            hostIp: 'N/A'
                        },
                        topic:BEELINE.ROAMING_INTERNET_LOGS_TOPIC,
                        key: record[USAGEID],

                    };
                    this.push(msg);
                }
                callback();
            }
        });
    }

    get transformer() {
        return this.transformUsageData;
    }
}

module.exports = UsageDataTransformStream;