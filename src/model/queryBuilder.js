const knex = require('knex')({client: 'oracle'});

const InternalKeys = require('../constants/internalKeys');

class QueryBuilder {
    constructor(task) {
        this.task = task;
        this.query = {
            queryString: '',
            queryParams: []
        };

        this.localBaselinesQueryFactory = {
            //Usage table
            'sms': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS')`;
                query += `AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.insideNetworkCall}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.insideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.outsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.internationalSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.paidServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.freeServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingOutsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInternationalSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingPaidServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingFreeServiceSms}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;


                this.query.queryString = query;
            },

            'calls': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS') AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.insideNetworkCall}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.outsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.internationalCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.householdPhoneCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.paidServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.freeServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingOutsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInternationalCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingHouseholdPhoneCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingPaidServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingFreeServiceCall}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },

            'internet': () => {

                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS') AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.internet}'`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },

            'allData': () => {

                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS') AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.insideNetworkCall}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.outsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.internationalCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.householdPhoneCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.paidServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.freeServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingOutsideNetworkCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInternationalCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingHouseholdPhoneCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingPaidServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingFreeServiceCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.insideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.outsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.internationalSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.paidServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.freeServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingOutsideNetworkSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingInternationalSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingPaidServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingFreeServiceSms}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.internet}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },
        };

        this.balanceBaselinesQueryFactory = {

            //Balance table

            'topup': () => {
                /*
                *   SELECT RECHARGE.RECHARGE_NVAL, BAL.ACCOUNT_KEY
                *   FROM RECHARGE
                *   INNER JOIN BAL ON RECHARGE.MSISDN = BAL.MSISDN;
                */

                this.query.queryString +=
                    knex
                        .select("RECHARGE.RECHARGE_NVAL", 'BAL.ACCOUNT_KEY')
                        .from('RECHARGE')
                        .innerJoin('BAL', 'RECHARGE_NVAL', '=', 'BAL.MSISDN');

                this.query.queryString = this.query.queryString.replace(/"/g, '');
            },

            'credit': () => {
                this.query.queryString += knex('TRUSTPAYMENT')
                    .where({
                        // SUBS_KEY: this.task['msisdn']
                    })
                    .select();
            },

            'remaining': () => {
                let query = `SELECT * FROM BAL WHERE `;
                query += `MSISDN = '${task['msisdn']}'`;

                this.query.queryString = query;
            }
        };

        this.roamingBaslinesQueryFactory = {
            'sms': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS')`;
                query += ` AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.incomingRoamingSms}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.roamingSms}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },

            'calls': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS')`;
                query += ` AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.incomingRoamingCall}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.roamingCall}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },

            'internet': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS')`;
                query += ` AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.roamingInternet}'`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            },

            'allData': () => {
                let tableData = this.task['task_data'];
                let fromDt = (tableData['date_from']).replace(/T/g, ':').replace(/Z/g, '');
                let toDt = (tableData['date_to']).replace(/T/g, ':').replace(/Z/g, '');

                let query = `SELECT * FROM FAKEUSAGE WHERE START_DT >= TO_DATE('${fromDt}', 'YYYY-MM-DD:HH24:MI:SS') AND "START_DT" <= TO_DATE('${toDt}', 'YYYY-MM-DD:HH24:MI:SS')`;
                query += ` AND ( `;
                query += `CALL_TYPE_KEY = '${InternalKeys.roamingSms}'`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.incomingRoamingCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.roamingCall}')`;
                query += `OR (CALL_TYPE_KEY = '${InternalKeys.roamingInternet}')`;
                query += `)`;
                query += `AND ( `;
                query += `PHONE_NUM = '${tableData.msisdn}'`;
                query += `)`;

                this.query.queryString = query;
            }
        }
    }

    /*
    * "MICRO_SERVICE_ID":"123",
    * "INSTANCE_ID":"222",
    * "status":"IN_QUEUE",
    * "data":
    * {
    *   "task_data":
    *           {"date_from":"2018-09-01T00:00:00Z",
    *           "date_to":"2018-01-12T00:00:00Z",
    *           "msisdn":"996779123123",
    *           "domain":"calls"}
    *   }
    * }
    * */

    build() {
        let tableData = this.task['task_data'];

        // Building and assigning query here.
        //
        // Not too clear what the code does
        // TODO refactoring a little bit...

        let domain = tableData['domain'].split('.');
        if (!domain[1])
            domain[1] = 'allData';

        if (domain[0] === 'balance')
            this.balanceBaselinesQueryFactory[domain[1]]();

        if (domain[0] === 'local')
            this.localBaselinesQueryFactory[domain[1]]();

        if (domain[0] === 'roaming')
            this.roamingBaslinesQueryFactory[domain[1]]();
    }

    get getQuery() {
        return this.query;
    }
}

module.exports = QueryBuilder;