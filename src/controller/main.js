const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');

const logger = require('askarteklogger');
const {Producer} = require('rib-kafka');
const {TaskConsumer} = require('watcher-js');

const Process = require('./processor');

require('dotenv').config({path: '.env'});
const routes = require('../../src/routes/routes');

class OracleResourceAcquirer {
    constructor() {
        this.componentName          = '';
        this.task_id                = 'Fetch process init';
        this.JOBS_LIMIT             = null;
        this.componentIdFilePath    = '';
        this.producerConfigs        = null;
        this.componentID            = '';
        this.watcher                = null;
        this.producer               = null;
        this.app                    = express();
        this.watcherConfigs         = null;
        this.jobsQueue              = [];
    }

    async init() {
        this.componentName                      = config.get('componentName');
        this.task_id                            = config.get('componentInittask_id');
        this.JOBS_LIMIT                         = config.get('JOBS_LIMIT');
        this.componentIdFilePath                = config.get('componentIdFilePath');
        this.producerConfigs                    = config.get('kafka');
        this.componentID                        = process.env.CONTAINER_ID;
        this.watcherConfigs                     = Object.assign({}, config.get('watcher'));
        this.watcherConfigs['MICRO_SERVICE_ID'] = this.componentID;
        this.watcher                            = new TaskConsumer(this.watcherConfigs);
        this.producer                           = new Producer({config: this.producerConfigs});

        this.producer.on('error', this.onError);
        // this.watcher.on('error', this.onError);

        await this.watcher.connect();
        await this.producer.connect();

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
        this.app.use('/', routes);
        this.app.set('port', process.env.PORT || 8080);
        const server = this.app.listen(this.app.get('port'), () => {
            console.log(`Express running → PORT ${server.address().port}`);
            logger.logInfo(`Express running → PORT ${server.address().port}`, this.task_id);
            this.watcher.consume(this.manageTasks.bind(this));
        })
    }


     // {
     // "MICRO_SERVICE_ID":"123",
     // "INSTANCE_ID":"222",
     // "status":"IN_QUEUE",
     // "data":
     // {
     //   'task_id':'123',
     //   "task_data":
     //           {"date_from":"2018-09-01T00:00:00Z",
     //           "date_to":"2018-01-12T00:00:00Z",
     //           "msisdn":"996779123123",
     //           "domain":"calls"}
     //   }
     // }


    static validateTaskData(task) {
        if(!task.hasOwnProperty('task_data'))
            throw new Error('Task must have a task_data field');

        if(!task.task_data.hasOwnProperty('domain'))
            throw new Error('Task must have a domain field');

        if(!task.task_data.hasOwnProperty('date_from'))
            throw new Error('Task must have a date_from field');

        if(!task.task_data.hasOwnProperty('msisdn'))
            throw new Error('Task must have a msisdn field');

        if(!task.task_data.hasOwnProperty('date_to'))
            throw new Error('Task must have a date_to field');
    }


    manageTasks(message, next) {
        let task = Object.assign({}, (message.value));

        console.log('Task:',JSON.stringify( message.value));
        try {
            OracleResourceAcquirer.validateTaskData(task.data);
        } catch (err) {
            logger.logError('Task did not pass validation. ' + err, message.key.toString());
            console.error('Task did not pass validation. ' + err + '. Task ID: ', message.key.toString());
            next();
            return;
        }

        task.data['task_id'] = message.key.toString();

        if(message.value.status === 'cancel'){
            this.cancelTask(task.data)
                .catch(err => logger.logError('Could not cancel process execution' + err, task.data['task_id']))
                .then(()=>{
                    // message.setStatusCancelled();
                    next();
                })
        }

        let a = setInterval(()=>{
            if (this.jobsQueue.length < this.JOBS_LIMIT){
                this.addTask(task.data, message, next);
                clearInterval(a);
            }
        }, 2000)
    }

    addTask(task, message, next) {
        let process = new Process(task, this.producer);
        this.jobsQueue.push(process);

        process
            .on('endProcess', () => {
                logger.logTrace('Task done', task.task_id);
                message.setStatusSuccess();
                this.removeTask(process.task);
            })
            .on('errorProcess', (err) => {
                logger.logError('Error processing task. Error: ' + err, task.task_id);
                console.error('Error processing task. Error: ' + err + task.task_id);
                message.setStatusError();
                this.removeTask(process.task);
            })
            .on('noDataStream', () => {
                logger.logError('Error processing task. Error: data fetcher returned empty stream', task.task_id);
                message.setStatusError();
                this.removeTask(process.task);
            });

        process.start()
            .then(() => {
                console.log('starting a task:', task.task_id);
                logger.logTrace('Starting task process ...', task.task_id);
                next();
            })
            .catch(err => {
                console.log('error starting a task:' + task.task_id + err);
                logger.logError('Error starting task process: ' + err, task.task_id);
                next();
            })
    }

    removeTask(task) {
        for (let i in this.jobsQueue) {
            if (task['task_id'] === this.jobsQueue[i].task['task_id']) {
                this.jobsQueue.splice(i, 1);
            }
        }
    }

    async cancelTask(task) {
        if(task.hasOwnProperty('task_id')) {
            //Searching for the job to be cancelled in running jobs
            for (let i in this.jobsQueue) {
                if (this.jobsQueue[i].task['task_id'] === task.task_id) {
                    await this.jobsQueue[i].cancelTask();
                    this.jobsQueue.splice(i, 1);
                }
            }
        }
    }

    onError(err) {
        logger.logError('Error: ' + err, this.task_id);
        console.error(err);
    }

    shutdown() {
        this.producer.disconnect();
        this.watcher.disconnect();
        for (let i in this.jobsQueue) {
            this.jobsQueue[i].shutdown();
        }
        logger.shutdown();
    }
}

module.exports = OracleResourceAcquirer;