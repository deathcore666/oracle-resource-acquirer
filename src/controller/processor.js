const EventEmitter = require('events');
const logger = require('askarteklogger');

const BEELINE = require('../constants/beeline');
const DataFetcher = require('../oracle/data-fetcher');
const QueryBuilder = require('../model/queryBuilder');

// Потом удалю
// let startTime;

/**
 * Information about user's balance is stored in three different views: 'payment', 'trustpayment' and 'balance'
 * Tasks that require info residing in those views has to have following structure
 * * balance.<remaining/credit/topup>
 */

class Processor extends EventEmitter {
    constructor(task, kafkaProducer) {
        super();
        this.task = task;
        this.dataFetcher = new DataFetcher(this.task);
        this.kafkaProducer = kafkaProducer;
        this.dataStream = null;
        this.queryBuilder = new QueryBuilder(this.task);
        this.query = {};
        this.topicSet = new Set();

        this.tableDataHandler = {
            'local': {
                'calls': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'sms': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'internet': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'tarificaion': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                }
            },

            'roaming': {
                'calls': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'sms': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'internet': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'tarificaion': async () => {
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                }
            },

            'balance': {
                'topup': async () => {
                    this.dataStream = await this.dataFetcher.fetchBalanceTopUpDataStream(this.query.queryString, this.query.queryParams);
                },

                'credit': async () => {
                    this.dataStream = await this.dataFetcher.fetchBalanceCreditDataStream(this.query.queryString, this.query.queryParams);
                },

                'remaining': async () => {
                    this.dataStream = await this.dataFetcher.fetchBalanceRemainingDataStream(this.query.queryString, this.query.queryParams);
                },

            },
        }
    }

    async start() {
        try {
            await this.dataFetcher.connect();
            this.queryBuilder.build();
            this.query = this.queryBuilder.getQuery;
            console.log(this.query.queryString);
            await this.fetchStream();
            this.sendData();
        } catch (err) {
            this.emit('errorProcess', err);
        }
    }

    async fetchStream() {
        logger.logDebug('Attempting to parse domain and fetch stream...', this.task.task_id);
        let domain = this.task['task_data']['domain'].split('.');
        await this.tableDataHandler[domain[0]][domain[1]]();
    }

    sendData() {
        //Мне нужен этот код для настроек Oracle
        //Потом удалю
        // startTime = Date.now();
        // console.log('this.dataStream:', this.dataStream);

        if (!this.dataStream) {
            this.dataFetcher.releaseConnection();
            this.emit('noDataStream');
            return;
        }

        this.dataStream.on('data', async (message) => {
            try {
                this.topicSet.add(JSON.stringify(message.topic));

                await this.kafkaProducer.produce(message.topic, null, new Buffer(JSON.stringify(message.data)), message.key, Date.now())
            } catch (err) {
                console.error('Error producing a message to kafka: ', err);
                logger.logWarn('Error producing a message to kafka: ' + err, this.task.task_id);
                await this.dataStream.end();
                await this.dataFetcher.releaseConnection();
                this.emit('errorProcess', err);
            }
        });

        this.dataStream.on('end', async () => {
            try {
                for (let topic in this.topicSet) {
                    await this.kafkaProducer.produce(topic, null, new Buffer(`{taskId: ${this.task.task_id}, EOF: 1}`), null, Date.now())
                }

            } catch (err) {
                console.error('Error producing an EOF message to kafka: ', err);
                logger.logWarn('Error producing an EOF message to kafka: ' + err, this.task.task_id);
                this.emit('errorProcess', err);
            }

            this.dataStream.end();
            this.dataFetcher.releaseConnection();
            this.emit('endProcess');

            // Этот тоже
            // let t = ((Date.now() - startTime)/1000);
            // console.log('queryStream():' + t);
        });

        this.dataStream.on('error', async (err) => {
            this.dataFetcher.releaseConnection();
            this.dataStream.end();
            this.emit('errorProcess', err);
        })
    }

    cancelTask() {
        logger.logTrace('Cancelling task...', this.task.task_id);
        this.dataStream.end();
        this.dataFetcher.releaseConnection();
    }

    shutdown() {
        if (this.dataStream)
            this.dataStream.end();

        this.dataFetcher.releaseConnection();
    }
}

module.exports = Processor;


/*
* {
*   taskId: 234234234234,
 *  EOF: "EOF"
 *  }
* */