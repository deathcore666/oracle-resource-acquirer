const express = require('express');
const route = express.Router();

route.use( (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

route.post('/health', (req, res) => res.status(200).json({ status: 'ok' }));

module.exports = route;