const logger = require('askarteklogger');

const configValidator = require('./config/config');
const ORA = require('./src/controller/main');

function init() {
    const ora = new ORA();
    const main = async () => {
        await configValidator.validate();
        await logger.init();
        await ora.init();

        process.once('SIGTERM', () => ora.shutdown());
        process.once('SIGINT', () => ora.shutdown());
        process.once('SIGHUP', () => ora.shutdown());
    };

    main()
        .then(() => {
            logger.logInfo('An instance of M1-1 ' + ora.componentID + 'has been started successfully.', ora.task_id);
            console.log('An instance of M1-1 ' + ora.componentID + ' has been started successfully.');
        })
        .catch(err => {
            console.error('Error on service init: ', err)
        })
}

init();
// setInterval(function(){}, 1000);