const config = require('config');

function validate() {
    return new Promise(function(resolve, reject) {
        let requiredList = config.get('main.configsList');
        for(let i in requiredList) {
            if(!config.has(requiredList[i]))
                reject(requiredList + ' is missing from the config file');
        }
        resolve({})
    })
}

module.exports.validate = validate;