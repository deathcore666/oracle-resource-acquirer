#!/usr/bin/env bash

echo >> .env
sed -i '/CONTAINER_ID/d' ./.env
sed -i '/^\s*$/d' .env
echo -n CONTAINER_ID= >> .env
echo `cat /proc/self/cgroup | grep name | cut -d \: -f 3 | cut -c 9-20` >> .env

npm start